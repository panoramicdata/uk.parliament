﻿using System;

namespace Uk.Parliament.Exceptions
{
	/// <summary>
	/// Missing payload response exception
	/// </summary>
	public class MissingPayloadResponseException : Exception
	{
	}
}