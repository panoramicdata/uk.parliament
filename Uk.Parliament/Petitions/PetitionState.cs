
namespace Uk.Parliament.Petitions
{
	/// <summary>
	/// Petition state
	/// </summary>
	public enum PetitionState
	{
		/// <summary>
		/// Open
		/// </summary>
		Open,

		/// <summary>
		/// Closed
		/// </summary>
		Closed,
	}
}